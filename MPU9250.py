from skidl import *

MPU9250 = Part("MPU-9250", "MPU-9250")

c = Part("Device", "C", dest=TEMPLATE)
r = Part("Device", "R", dest=TEMPLATE)
vcct = Part("power", "VCC", dest=TEMPLATE)
gndt = Part("power", "GND", dest=TEMPLATE)

# pin18 pin20
A = Net()
MPU9250['18', '20'] += A
gndt() & A

# pin13
B = Net()
B & vcct() & tee(c() & gndt())
MPU9250['13'] += B

# pin22
# MPU9250['CS/TP3'] & r() & vcct()
MPU9250['22'] & r() & vcct()

# pin1
# MPU9250['RESV/CCS'] & vcct()
MPU9250['1'] & vcct()

# pin10 pin11
g = gndt()
MPU9250['10'] & tee(c() & g)
# MPU9250['FSYNC/TP1'] & g
MPU9250['11'] & g

# pin8
MPU9250['8'] & vcct()

ERC()
generate_svg()