# U1C
from skidl import *
MCU = Part("EFR32BG12P332F1024GL125","efr32bg12p332f1024gl125")
#N = NCNet()
#for p in MCU.pins:
#    p+=N
c = Part("Device", "C", dest=TEMPLATE)
r = Part("Device", "R", dest=TEMPLATE)
vcct = Part("power", "VCC", dest=TEMPLATE) 
gndt = Part("power", "GND", dest=TEMPLATE) 

@subcircuit
def parallelC(output):
    g = gndt()
    vcct() & tee(c() & g) & tee(c() &  g) & output

@subcircuit
def singleC(output):
    vcct() & tee(c() &  gndt()) & output

# Digital Supply
parallelC(MCU['VREGVDD'])
# Analog Supply 
parallelC(MCU['AVDD'])
# I/O Supply
A = Net()
MCU['M12', 'B10', 'F2', 'F11'] += A
parallelC(A)
# Digital Regulator
singleC(MCU['DVDD'])
# Digital Logic
MCU['DECOUPLE'] & c.copy() & gndt.copy()
# Ground
ground = Net()
MCU['A13', 'B11', 'B12'] += ground
ground & gndt.copy()
ERC()
generate_svg()